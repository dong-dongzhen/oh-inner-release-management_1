### 分支关系

为了保障OpenHarmony社区版本的平滑演进和历史发布版本的持续稳定可靠，会定期从Master主干拉出LTS/release/Beta等类型分支，并按照OpenHarmony生命周期定义对其进行管理。

![branches.png](https://gitee.com/openharmony-sig/oh-inner-release-management/raw/master/Maintenance/branches.png "branches.png")

#### Master （主干分支）

master作为openHarmony社区持续滚动开发的主干，积极接纳社区每个软件包主干开发分支的代码更新，将其纳入主干实时构建。

####  LTS分支（长期支持维护分支）

openHarmony社区LTS长期支持维护分支按照每12个月一个周期在每年的Q3季度从master主干分支拉出来。该LTS分支版本在经过集中编译、构建、集成测试，并最终通过社区评审发布。

#### release分支（发布分支）

openHarmony社区release分支按照每12个月一个周期在每年的Q1季度从master主干分支拉出来。该分支版本在经过集中编译、构建、集成测试，并最终通过社区评审发布。其与LTS分支的发布要求一致，但其维护周期短于LTS分支。

#### Beta分支（测试分支）

openHarmony社区Beta分支是在社区开发和演进过程中不定期从master主干分支拉出来。该分支仅完成集中编译、构建、基础功能验证，可用于开发调试。

#### 标签版本

openHarmony社区基于LTS/release分支以patch形式合入少量补丁代码，用于解决单点bug、安全漏洞、以及其他必须的适配修改，经过集成验证之后发布的稳定可靠的标签版本。







